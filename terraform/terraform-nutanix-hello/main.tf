// VM DISK IMAGE
resource "nutanix_image" "centos" {
  name        = "CENTOS_CLOUD"
  description = "Centos Generic Cloud - Imported by Terraform"
  source_uri  = "https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud-1805.qcow2"
}

// Generate the cloud-init file
data "template_file" "init" {
    template = "${file("${path.module}/assets/cloud-init.yml")}"
}

data "template_cloudinit_config" "config" {
    gzip          = false
    base64_encode = true

    part {
        content = "${data.template_file.init.rendered}"
    }
}

// Generate the service unit file
data "template_file" "service" {
  template = "${file("${path.module}/assets/hello.service")}"

  vars {
    message = "${var.message}"
  }
}

// VM
resource "nutanix_virtual_machine" "hello" {

    name                 = "hello-nutanix-${timestamp()}"
    description          = "A demonstration of the Nutanix Terraform provider"
    num_sockets          = 1
    num_vcpus_per_socket = 2
    memory_size_mib      = 512

    cluster_reference {
        kind = "cluster"
        uuid = "${var.cluster}"
    }

    nic_list "subnet_reference" {
        kind = "subnet"
        uuid = "${var.network}"
    }

    disk_list "data_source_reference" {
        kind = "image"
        uuid = "${nutanix_image.centos.id}"
    }

    guest_customization_cloud_init_user_data = "${data.template_cloudinit_config.config.rendered}"

    connection {
        type        = "ssh"
        user        = "centos"
        private_key = "${file("${path.module}/assets/ssl/demo_key")}"
        host        = "${nutanix_virtual_machine.hello.nic_list.0.ip_endpoint_list.0.ip}"
    }

    provisioner "file" {
        source      = "${path.module}/assets/app/greeting.tmpl.html"
        destination = "/home/centos/greeting.tmpl.html"
    }

    provisioner "file" {
        content     = "${data.template_file.service.rendered}"
        destination = "/home/centos/hello.service"
    }

    provisioner "file" {
        source      = "${path.module}/assets/app/hello"
        destination = "/home/centos/hello"
    }

    provisioner "remote-exec" {
        inline = [
            "sudo mv /home/centos/hello /usr/local/bin/hello",
            "sudo mv /home/centos/hello.service /etc/systemd/system/hello.service",
            "sudo chmod +x /usr/local/bin/hello",
            "sudo systemctl start hello.service",
            "sudo systemctl enable hello.service"
        ]
    }

}